// Belajar percabangan kondisi
function buttonclick() {
    let materi2 = prompt('Mau belajar apa sekarang?');
    let materi3 = materi2.toLowerCase()
    console.log(materi3);

    if (materi3 == 'javascript') {
        alert('Benar! Kamu masuk kelas yang benar');
    }else {
        alert('Kamu salah kelas!');
    }
}

// Belajar Aritmatika Dasar
function buttonclick2() {
    let angka1 = prompt('Masukkan angka1:');
    let angka2 = prompt('Masukkan angka2:');
    let angka3 = prompt('Masukkan angka3:');

    alert(Number(angka1) * Number(angka2) + Number(angka3));
}

// Belajar Confirm
function buttonclick3() {
    let istri = prompt('Masukkan nama istrimu!');
    let isAgain;

    if (istri == 'jennie') {
        alert('Betul, itu istrimu!');
    } else {
        alert('Bukan, itu bukan istrimu!');
        isAgain = confirm('Coba lagi!');
        if (isAgain === true) location.reload();
    }
}

// Kalkulator sederhana
function kalkulator() {

    let operasi = prompt('Pilih operasi kalkulator! tambah/kurang/kali/bagi?');
    let isAgain2;

    if (operasi == 'tambah'){
        let nilai1 = prompt('Masukkan angka ke-1!');
        let nilai2 = prompt('Masukkan angka ke-2!');
        alert(Number(nilai1) + Number(nilai2));
    }
    else if(operasi == 'kurang'){
        let nilai1 = prompt('Masukkan angka ke-1!');
        let nilai2 = prompt('Masukkan angka ke-2!');
        alert(Number(nilai1) - Number(nilai2));
    }
    else if(operasi == 'kali'){
        let nilai1 = prompt('Masukkan angka ke-1!');
        let nilai2 = prompt('Masukkan angka ke-2!');
        alert(Number(nilai1) * Number(nilai2));
    }
    else if(operasi == 'bagi'){
        let nilai1 = prompt('Masukkan angka ke-1!');
        let nilai2 = prompt('Masukkan angka ke-2!');
        alert(Number(nilai1) / Number(nilai2));
    }
    else {
        isAgain2 = confirm('Anda salah pilih operasi! Coba lagi!');
        if (isAgain2 === true) { 
            location.reload(true);
            document.getElementById("myBtn").click(); 
        }
    }
}